import {fetchInObject} from "lkt-object-tools";
import {LktObject} from "lkt-ts-interfaces";

export class DigToMaxPageValue {
  public readonly value: string;

  constructor(value: string) {
    if (!value) value = '';
    this.value = value;
  }

  hasToDig() {
    return this.value !== '';
  }
  
  dig(data: LktObject) {
    return fetchInObject(data, this.value);
  }
}
